
import dlib
import cv2
import numpy as np
import func
import os


class FaceEngine:

    def __init__(self):
        self.my_dir = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
        self.face_csv = 'face_db.csv'
        self.img_face_path = 'img_faces'

        self.detector = dlib.get_frontal_face_detector()
        self.predictor = dlib.shape_predictor(os.path.join(self.my_dir, 'models/dlib_model.dat'))
        self.recognizer = dlib.face_recognition_model_v1(
            os.path.join(self.my_dir, "models/dlib_face_recognition_resnet_model_v1.dat"))

    def get_landmarks(self, im, rects):
        points_list = []

        if len(rects) != 0:
            for i in range(len(rects)):
                predict_ret = self.predictor(im, rects[i]).parts()
                points = []
                for p in predict_ret:
                    points.append((p.x, p.y))

                points_list.append(points)

            return points_list

        else:
            return None

    def __get_face_rect(self, im):
        rect = self.detector(im, 0)
        return rect

    def get_face(self, im):
        face_list = []
        coordinate_list = []
        rect = self.__get_face_rect(im)

        for i, d in enumerate(rect):
            x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()
            im_face = im[y1:y2, x1:x2]
            face_list.append(im_face)
            coordinate_list.append([x1, x2, y1, y2])

        return rect, face_list, coordinate_list

    def _recognize_face(self, frame, rect):
        shape = self.predictor(frame, rect)
        face_description = self.recognizer.compute_face_descriptor(frame, shape)
        return np.asarray(face_description)

    def get_feature128(self, frame, dets):

        descriptions = []

        for k, r in enumerate(dets):
            face_description = self._recognize_face(frame, r)
            descriptions.append(np.asarray(face_description))

        return descriptions

    def enroll_image(self, img_file, face_name):

        img = cv2.imread(img_file)

        if img is None:
            return False

        rect, face_list, coordinate_list = self.get_face(img)
        feature128_list = self.get_feature128(img, rect)

        if len(feature128_list) > 0:
            # ------------ write csv headers ------------
            if not os.path.isfile(self.face_csv):
                title = ['First Name', 'Sure Name', 'Staff ID', 'Student ID']
                for i in range(128):
                    title.append('Feature' + str(i+1))
                func.save_csv(self.face_csv, [title])

            # -------------- write face info ------------
            rec_data = face_name
            for i in range(128):
                rec_data.append(feature128_list[0][i])

            func.append_csv(self.face_csv, [rec_data])

            # ------------- save face images --------------
            if not os.path.isdir(self.img_face_path):
                os.mkdir(self.img_face_path)

            file_face = face_name[0] + '_' + face_name[1] + '_' + face_name[2] + '_' + face_name[3]
            cv2.imwrite(self.img_face_path + '/' + file_face + '.jpg', face_list[0])

            return True
        else:
            return False

    def check_image(self, img_file):

        img = cv2.imread(img_file)

        if img is None:
            return None

        rect, face_list, coordinate_list = self.get_face(img)
        feature128_list = self.get_feature128(img, rect)

        if len(feature128_list) > 0:
            face_data = func.load_csv(self.face_csv)
            face_name_list = []
            match_list = []

            for i in range(1, len(face_data)):
                face_name_list.append(face_data[i][:4])
                face_feature = []
                for j in range(128):
                    face_feature.append(face_data[i][j+4])

                match_list.append(func.get_match_value(feature128_list[0], face_feature))

            match_id = np.argmax(match_list)
            return [face_name_list[match_id], match_list[match_id]]

        else:
            return None


if __name__ == '__main__':
    class_face = FaceEngine()
    image = 'img_src/w2.jpg'

    # class_face.enroll_image(image, 'henry')
    class_face.check_image(image)
