
from kivy.app import App
from kivy.config import Config
from kivy.lang import Builder
from kivy.properties import ListProperty, StringProperty
from kivy.clock import Clock
from kivy.graphics.texture import Texture
from face_engine import FaceEngine
import sys
import cv2
import os


class BestApp(App):

    screen_names = ListProperty([])
    screens = {}  # Dict of all screens

    title = StringProperty()
    txt_file_name = StringProperty()
    txt_first_name = StringProperty()
    txt_last_name = StringProperty()
    txt_staff_id = StringProperty()
    txt_student_id = StringProperty()
    label_match = StringProperty()
    msg = StringProperty()

    def __init__(self, **kwargs):

        self.class_face = FaceEngine()

        self.title = 'Face Recognition'
        self.img_face = 'logo/3.jpg'
        self.temp_img = 'temp.jpg'
        self.label_match = '0.0'
        self.msg = 'Welcome!'

        self.red = (0, 0, 255)
        self.blue = (255, 0, 0)
        self.white = (255, 255, 255)

        self.event_take_video = None
        self.capture = None
        self.frame = None
        self.fps = 30

        self._init_cv()
        self.on_resume()

        super(BestApp, self).__init__(**kwargs)

    # ----------------------------- Button Events -------------------------------
    def on_exit(self):
        self.on_stop()
        self.capture.release()
        sys.exit()

    def on_web_cam(self):
        self.img_face = '0'
        self._init_cv()
        self.on_resume()

    def on_file_open(self):
        self.title = 'File Open'
        self.go_screen('dlg_file', 'left')
        self.on_stop()

    def on_file_select(self, state, file_name):
        if state == 'sel' and file_name != []:
            self.txt_file_name = file_name[0]
            self.img_face = file_name[0]

        self.title = 'Face Recognition'
        self.go_screen('dlg_enroll', 'right')
        self._init_cv()
        self.on_resume()

    def on_enroll(self, txt_first_name, txt_last_name, txt_staff_id, txt_student_id):
        face_info = [txt_first_name, txt_last_name, txt_staff_id, txt_student_id]
        if self.img_face == '0':
            cv2.imwrite(self.temp_img, self.frame)
            ret = self.class_face.enroll_image(self.temp_img, face_info)
        else:
            ret = self.class_face.enroll_image(self.img_face, face_info)

        if ret:
            self.msg = 'Enrolled successfully!'
        else:
            self.msg = 'Invalid image or no face!'

    def on_check(self):
        if self.img_face == '0':
            cv2.imwrite(self.temp_img, self.frame)
            ret_check = self.class_face.check_image(self.temp_img)
        else:
            ret_check = self.class_face.check_image(self.img_face)

        if ret_check is None:
            self.msg = 'Invalid image or no face!'
        else:
            if ret_check[1] > 96:
                self.txt_first_name = ret_check[0][0]
                self.txt_last_name = ret_check[0][1]
                self.txt_staff_id = ret_check[0][2]
                self.txt_student_id = ret_check[0][3]
                self.msg = "Authorized successfully!"
            else:
                self.msg = "Unknown person!"
            # self.label_match = str(round(ret_check[1], 2))

    """ --------------------------------- Image Processing ----------------------------------- """
    def _init_cv(self):
        if self.img_face == '0':
            self.capture = cv2.VideoCapture(0)
        else:
            self.capture = cv2.VideoCapture(self.img_face)

        if not self.capture.isOpened():
            print('Failed to get source of CV')
        else:
            self.frameWidth = int(self.capture.get(cv2.CAP_PROP_FRAME_WIDTH))
            self.frameHeight = int(self.capture.get(cv2.CAP_PROP_FRAME_HEIGHT))

    def get_frame(self, *args):
        ret, frame = self.capture.read()

        if ret:
            self.frame = frame

            self.frame_to_buf(frame=frame)
        else:
            self.root.ids.img_face.source = 'logo/3.jpg'
            self.event_take_video.cancel()
            Clock.unschedule(self.event_take_video)
            self.event_take_video = None

    def frame_to_buf(self, frame):
        frame = cv2.resize(frame, (self.frameWidth, self.frameHeight))
        buf1 = cv2.flip(frame, 0)
        buf = buf1.tostring()
        self.root.ids.img_face.texture = Texture.create(size=(self.frameWidth, self.frameHeight))
        self.root.ids.img_face.texture.blit_buffer(buf, colorfmt='bgr', bufferfmt='ubyte')

    def on_resume(self):
        if self.event_take_video is None:
            self.event_take_video = Clock.schedule_interval(self.get_frame, 1.0 / self.fps)
        elif not self.event_take_video.is_triggered:
            self.event_take_video()

    def on_stop(self):
        if self.event_take_video is not None and self.event_take_video.is_triggered:
            self.event_take_video.cancel()

    # ------------------------------ system setting --------------------------------
    def build(self):
        self.load_screen()
        self.go_screen('dlg_enroll', 'right')

    def go_screen(self, dest_screen, direction):
        sm = self.root.ids.sm
        sm.switch_to(self.screens[dest_screen], direction=direction)

    def load_screen(self):
        self.screen_names = ['dlg_enroll', 'dlg_file']

        for i in range(len(self.screen_names)):
            screen = Builder.load_file('kv_dlg/' + self.screen_names[i] + '.kv')
            self.screens[self.screen_names[i]] = screen
        return True


if __name__ == '__main__':
    Config.set('graphics', 'width', '1000')
    Config.set('graphics', 'height', '600')
    Config.set('graphics', 'resizable', 0)
    Config.set('kivy', 'window_icon', 'logo/logo_5Zc_icon.ico')
    BestApp().run()
