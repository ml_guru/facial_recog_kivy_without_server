from face_engine import FaceEngine
import sys

class_face = FaceEngine()

in_args = ['1.jpg', '-c', 'name']

for i in range(len(sys.argv) - 1):
    in_args[i] = sys.argv[i + 1]

file_name = in_args[0]
method = in_args[1]
face_name = in_args[2]

if method == '-c' or method == 'check':
    class_face.check_image(file_name)
elif method == '-e' or method == 'enroll':
    class_face.enroll_image(file_name, face_name)
else:
    print "Invalid input parameter."
